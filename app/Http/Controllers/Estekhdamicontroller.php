<?php

namespace App\Http\Controllers;

use App\Models\Darkhast;
use App\Models\Repository;
use App\Models\Role;
use App\Models\RoleEstekhdam;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Estekhdamicontroller extends Controller
{
    public function request(){
        $roles = Role::all();
        return view('Estekhdami.request', compact('roles'));
    }
    public function  store(Request $request){ //sabt darkhast mooder
         
        $access = ['مدیر'];
        $c=User::where('username',Auth::user()->username)->first();
            if (!in_array( $c->roles()->first()->title, $access))
                abort(403, 'شما دسترسی به این صفحه ندارید');
                
        $request->validate([
            'comment' => 'required',
            'role' => 'required',
        ]);
        $req=Darkhast::where('user_username',Auth::user()->username)->delete();
        $req = new Darkhast();
        $req->comment = $request->comment;
        $req->user_username = Auth::user()->username;
        $req->save();
        // save role
        foreach (Role::all()  as $role1) {
        if (in_array($role1->id, $request->get('role'))) {
        $req = new RoleEstekhdam();
        $req->title =$role1->title;
        $D_Id=Darkhast::where('user_username', Auth::user()->username)->where('comment',$request->comment)->first();
        $req->Darkhast_id =  $D_Id->id;
        $req->save();
}}
         return redirect()->to('/Estekhdami/request')->with('message','درخواست شما با موفقیت ذخیره شد');
    }






}


