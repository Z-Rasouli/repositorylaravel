<?php

namespace App\Http\Controllers;

use App\Models\ExitKala;
use App\Models\Kala;
use App\Models\Product;
use App\Models\Unit_product;
use App\Models\User;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;

class ExitkalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function kalaExit(Request $request)
    {     //search date and fnameKala
        if ($request->date == '---' && $request->fname == '---')
            return redirect()->to('/products/exit')->with('message', 'چستجو براساس چی باشه!');
        $units = Unit_product::all();
        $kala = Kala::where(['repository_id' => Auth::user()->repository_id])->first();
        //fname and date fied search
        $productsFname = [];
        $findIds = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['kala_id']);
        $kalas = Kala::all();
        foreach ($kalas as $k => $find_fname) {
            foreach ($findIds as $k => $findId) {
                if ($find_fname->id == $findId->kala_id)
                    $productsFname[$k] = $find_fname->kala;
            }
        }
        $productsCreated = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);
        //end search

        // start convert date
        $a = [];
        $b = [];
        $i = 0;
        foreach ($productsCreated as $k => $row) {
            $v = verta($row->created_at);
            $f = Verta::getGregorian($v->year, $v->month, $v->day);
            $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
        }
        foreach ($a as $row) {
            if (!in_array($row, $b)) {
                $b[$i] = $row;
                $i++;
            }
        }
        $productsCreated = $b;
        // end conver date
        $fnameKalaExit = Kala::where('kala', $request->fname)->first();
        if ($request->date != '---' && $request->fname != '---')
            $exitKalas = ExitKala::where(['user_username' => Auth::user()->username, 'kala_id' =>  $fnameKalaExit->id])->where('created_at', 'like', $request->date . '%')->latest()->paginate(5);
        elseif ($request->fname != '---')
            $exitKalas = ExitKala::where(['user_username' => Auth::user()->username, 'kala_id' => $fnameKalaExit->id])->latest()->paginate(5);
        else  //($request->date != '---')
            $exitKalas = ExitKala::where('user_username', Auth::user()->username)->where('created_at', 'like',  $request->date . '%')->latest()->paginate(5);

        $kala = new Kala(); //clear field fname form
        $kala->kala = "";
        $test = "انتخاب کن";
        return view('products.ExitKala', compact('units', 'exitKalas', 'productsFname', 'productsCreated', 'kala', 'kalas','test'));
    }


    public function search_ExitKala(Request $request)
    {

        $units = Unit_product::all();
        $products = Product::where('user_username', Auth::user()->username)->orderBy('created_at', 'desc')->paginate(5);
        $kala = Kala::where(['repository_id' => Auth::user()->repository_id, 'id' => $request->code])->first();
        $productsFname = [];
        $findIds = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['kala_id']);
        $kalas = Kala::all();
        foreach ($kalas as $k => $find_fname) {
            foreach ($findIds as $k => $findId) {
                if ($find_fname->id == $findId->kala_id)
                    $productsFname[$k] = $find_fname->kala;
            }
        }
        $productsCreated = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);
        $exitKalas = ExitKala::where('user_username', Auth::user()->username)->where('created_at', 'like',  date("Y-m-d") . '%')->latest()->paginate(5);

        // start convert date
        $a = [];
        $b = [];
        $i = 0;
        foreach ($productsCreated as $k => $row) {
            $v = verta($row->created_at);
            $f = Verta::getGregorian($v->year, $v->month, $v->day);
            $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
        }
        foreach ($a as $row) {
            if (!in_array($row, $b)) {
                $b[$i] = $row;
                $i++;
            }
        }
        $productsCreated = $b;

        // end conver date
        if ($kala != null) {
            $test = Product::where('kala_id', $request->code)->first();
            $test = $test->unit_product_title;
            return view('products.ExitKala', compact('units', 'exitKalas', 'products', 'productsFname', 'productsCreated', 'kala', 'kalas', 'test'));
        } else {
            $test = "انتخاب کن";
            $kala = new Kala();
            $kala->kala = "یافت نشد";
            return view('products.ExitKala', compact('units', 'exitKalas', 'products', 'productsFname', 'productsCreated', 'kala', 'kalas', 'test'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $c = User::where('username', Auth::user()->username)->first();
        $acces = [];
        foreach ($c->roles as $k => $row)
            $acces[$k] = $row->title;
        if (!in_array('انباردار', $acces))
            abort(403, 'شما دسترسی به این صفحه ندارید');
        // *************************
        $kala = new Kala();
        $kala->kala = "";
        $units = Unit_product::all();
        // $products = Product::where('user_username', Auth::user()->username);   //->orderBy('created_at', 'desc')->paginate(5);
        $kala = Kala::where(['repository_id' => Auth::user()->repository_id])->first();
        $productsFname = [];
        $findIds = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['kala_id']);
        $kalas = Kala::all();
        foreach ($kalas as $k => $find_fname) {
            foreach ($findIds as $k => $findId) {
                if ($find_fname->id == $findId->kala_id)
                    $productsFname[$k] = $find_fname->kala;
            }
        }
        $productsCreated = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);

        $exitKalas = ExitKala::where('user_username', Auth::user()->username)->where('created_at', 'like',  date("Y-m-d") . '%')->latest()->paginate(5);
        // start convert date
        $a = [];
        $b = [];
        $i = 0;
        foreach ($productsCreated as $k => $row) {
            $v = verta($row->created_at);
            $f = Verta::getGregorian($v->year, $v->month, $v->day);
            $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
        }
        foreach ($a as $row) {
            if (!in_array($row, $b)) {
                $b[$i] = $row;
                $i++;
            }
        }
        $productsCreated = $b;
        // end conver date
        $kala = new Kala();   //clear field fnameKala
        $kala->kala = "";
        $test = "انتخاب کن";

        return view('products.ExitKala', compact('units', 'exitKalas', 'productsFname', 'productsCreated', 'kala', 'kalas', 'test'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'fname' => 'required',
            'count_original' => 'required|numeric',
        ]);
        $kala = new Kala();
        $kala->kala = "";
        $kala1 = Kala::where(['kala' => $request->fname, 'repository_id' => Auth::user()->repository_id])->first();
        // return $kala1;
        if ($kala1 != null) { //if kala exist
            $exitkala_count = ExitKala::where('kala_id', $kala1->id)->sum('count_original');
            $product_count = Product::where('fname', $kala1->kala)->sum('count_original');

            if (($exitkala_count + $request->count_original) > $product_count)
                return redirect()->back()->withErrors('تعداد باقی مانده موجودی:' . ($product_count - $exitkala_count));
            $exitKala = new ExitKala();
            $exitKala->kala_id = $kala1->id;
            $exitKala->count_original = $request->count_original;
            $exitKala->unit_product_title = $request->unit_product_title;
            $exitKala->comment = $request->comment;
            $exitKala->user_username = Auth::user()->username;
            $exitKala->kala_id = $kala1->id;
            $exitKala->save();
            $units = Unit_product::all();

            $kalas = Kala::all();
            $exitKalas = ExitKala::where('user_username', Auth::user()->username)->where('created_at', 'like',  date("Y-m-d") . '%')->latest()->paginate(5); //change

            $productsFname = [];
            $findIds = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['kala_id']);
            // $kalas=Kala::all();
            foreach ($kalas as $k => $find_fname) {
                foreach ($findIds as $k => $findId) {
                    if ($find_fname->id == $findId->kala_id)
                        $productsFname[$k] = $find_fname->kala;
                }
            }
            $productsCreated = ExitKala::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);
            // start convert date
            $a = [];
            $b = [];
            $i = 0;
            foreach ($productsCreated as $k => $row) {
                $v = verta($row->created_at);
                $f = Verta::getGregorian($v->year, $v->month, $v->day);
                $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
            }
            foreach ($a as $row) {
                if (!in_array($row, $b)) {
                    $b[$i] = $row;
                    $i++;
                }
            }
            $productsCreated = $b;
            // end conver date
            $test = "انتخاب کن";
            return view('products.ExitKala', compact('units', 'exitKalas', 'productsFname', 'productsCreated', 'kala', 'kalas','test'));
        }
        return redirect()->back()->withErrors('محصول در انبار معرفی نشده است');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
