<?php

namespace App\Http\Controllers;

use App\Models\Darkhast;
use App\Models\Estekhdam;
use App\Models\Repository;
use App\Models\Role;
use App\Models\RoleEstekhdam;
use App\Models\Save_dakhast_esthdam;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserEstekhdamcontroller extends Controller
{
    public function save_user(Request $request){ //store

        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'meli_code' => 'required|digits:10',
            'mobile' => 'required|regex:/(09)[0-9]{9}/',
            'comment'=>'max:500',
          ]);
          $meli_code=Estekhdam:: find($request->meli_code);
            if ($meli_code!=null)
            return 'صاحب این کد ثبت نام کرده است حال وارد بخش تکمیل فرم استخدام بشوید ';
      $Estekhdam_user =new  Estekhdam();
        $Estekhdam_user->meli_code = $request->meli_code;
             $Estekhdam_user->fname =$request->fname;
             $Estekhdam_user->lname =$request->lname;
             $Estekhdam_user->mobile=$request->mobile;
             $Estekhdam_user->Address= $request->Address;
             $Estekhdam_user->comment= $request->comment;
           $Estekhdam_user ->save();
           return redirect()->to('/Estekhdami/'.$request->meli_code.'/requisition_save');
    }

    public function code_meli_befor(Request $request){ //check code_meli befor rigister


        $request->validate([
            'meli_code_r' => 'required|digits:10',
          ]);
          $meli_code=Estekhdam:: find($request->meli_code_r);
          if ($meli_code!=null)
          return redirect()->to('/Estekhdami/'.$request->meli_code_r.'/requisition_save');
          else
         echo '<b>'.'<center>'.'نام کاربری موجود نیست'.'</center>'.'</b>';

        }
    public function requisition_person(){ // darkhast person for estekhdam
        $check=Darkhast::all();
        if ($check->isEmpty())
        return redirect()->to('Estekhdami/check');
        $AnbarName=[];
        $commentDarkhast=[];
        $darkhasts=Darkhast::all();
        foreach(  $darkhasts as $k=>$darkhast){
            $users=User::where('username',$darkhast->user_username)->get();
            foreach(  $users as $user){
            $AnbarNames[$k]=Repository::where('id',$user->repository_id)->first();
            $Darkhast=Darkhast::where('user_username',$user->username)->first();
            $commentDarkhast[$k]=$Darkhast->comment;
            $roles[$k]=RoleEstekhdam::where('Darkhast_id',$Darkhast->id)->get();
        }
        }
            return view('Estekhdami.new',compact('AnbarNames','commentDarkhast','roles'));
        }

        public function check(){  //check estkhdami is or not
            return view('Estekhdami.check');
        }

        public function requisition_save($id){


            $person=Estekhdam::find($id);
            if($person==null)
            return redirect()->to('Estekhdami/requisition_person');
        $check=Darkhast::all();
        if ($check->isEmpty())
        return redirect()->to('Estekhdami/check');
        $AnbarName=[];
        $commentDarkhast=[];
        $darkhastUsername=[];
        $darkhasts=Darkhast::all();
        foreach(  $darkhasts as $k=>$darkhast){
            $users=User::where('username',$darkhast->user_username)->get();
            foreach(  $users as $user){
            $AnbarNames[$k]=Repository::where('id',$user->repository_id)->first();
            $Darkhast=Darkhast::where('user_username',$user->username)->first();
            $commentDarkhast[$k]=$Darkhast->comment;
            $darkhastUsername[$k]=$darkhast->user_username;
            $roles[$k]=RoleEstekhdam::where('Darkhast_id',$Darkhast->id)->get();
        }
        }

            return view('Estekhdami.requisition_save',compact('AnbarNames','commentDarkhast','roles','person','darkhastUsername'));
        }

        public function save_formEstakhdami(Request $request){

              $AnbarID=Repository::where('title', $request->AnbarName)->first();
              $darkhastUsername=Darkhast::where('user_username',$request->darkhastUsername)->first();
              $roles=RoleEstekhdam::where('Darkhast_id',$darkhastUsername->id)->get();
              foreach ($roles  as $role1) {
                  if (in_array($role1->id, $request->get('role'))) {
                    $repeat=Save_dakhast_esthdam::where(['darkhast_id'=>$AnbarID->id,'role_id'=>$role1->id,'estekdam_meli'=>$request->person])->first();
                    if($repeat!=null)
                    return redirect()->back()->withErrors('درخواست شما ثبت شده است');
                    $save_estkhdam = new Save_dakhast_esthdam();
                    $save_estkhdam->darkhast_id= $darkhastUsername->id;
                    $save_estkhdam->role_id=$role1->id;
                    $save_estkhdam->estekdam_meli=$request->person;
                    $save_estkhdam->save();
                }
            }
            return redirect()->back()->withErrors('درخواست شما ثبت  گردید');
        }

        //vizit Estekhdami
        public function vizit(){
            $state=[];
            $estekhdams=[];
            $roles=[];
            $IdModer=Darkhast::where('user_username',Auth::user()->username)->first();
            if( $IdModer==null)
            return "درخواستی  داده نشده است";
            $saveDarkhasts=Save_dakhast_esthdam::where('darkhast_id' ,$IdModer->id)->orderBy('estekdam_meli')->get();
            if( $saveDarkhasts==null)
            return "تقاضایی  داده نشده است";
            foreach( $saveDarkhasts as $r=>$saveDarkhast){
            $estekhdams[$r]=Estekhdam::where('meli_code',$saveDarkhast->estekdam_meli)->orderBy('meli_code')->first();
           }
           $originalLinks=[];
foreach ($estekhdams as $link){
    if(!in_array($link,$originalLinks)){
        array_push($originalLinks,$link);
    }
}
$c=$estekhdams;
$estekhdams =$originalLinks;
            $i=0;
            foreach( $estekhdams as $k=>$estekhdam){
                $sb=Save_dakhast_esthdam::where(['estekdam_meli'=>$estekhdam->meli_code,'darkhast_id'=>$IdModer->id])->get();
                // echo  $state[$i];
                foreach( $sb as $s){
                $state[$i]=$s;
                $roles[$i]=RoleEstekhdam::find($state[$i]->role_id);
                $i++;
            }
        }
        $estekhdams =$c;
            return view('Estekhdami.vizit',compact('estekhdams','state','IdModer','roles'));
        }

        //save vizit
        public function saveVizit(Request $request){

            if($request->get('state')==null)
            return redirect()->to('/Estekhdami/vizit');
            $wait='0';
            $contact='0';
            $active='0';
            if (in_array(1, $request->get('state'))){
            $wait="1";

            }
            if (in_array(2, $request->get('state')))
            $contact="2";
            if (in_array(3, $request->get('state')))
            $active= "3";

            $update1 = Save_dakhast_esthdam::where(['estekdam_meli'=>$request->meli,'darkhast_id'=>$request->IdModer,'role_id'=>$request->roleperson])->update([
                'wait' => $wait,
                'contact' => $contact,
                'active' => $active,
            ]);

            return redirect()->to('/Estekhdami/vizit');
        }

        //searchState
        public function searchState(Request $request){

            $state=[];
            $estekhdams=[];
            $roles=[];
            $IdModer=Darkhast::where('user_username',Auth::user()->username)->first();

            if($request->checkState==3)
            $saveDarkhasts=Save_dakhast_esthdam::where(['darkhast_id' =>$IdModer->id,'active'=>3])->orderBy('estekdam_meli')->get();
            if($request->checkState==2)
            $saveDarkhasts=Save_dakhast_esthdam::where(['darkhast_id' =>$IdModer->id,'contact'=>2])->orderBy('estekdam_meli')->get();
            if($request->checkState==1)
            $saveDarkhasts=Save_dakhast_esthdam::where(['darkhast_id' =>$IdModer->id,'wait'=>1])->orderBy('estekdam_meli')->get();
            if($request->checkState==0)
            $saveDarkhasts= Save_dakhast_esthdam::where('darkhast_id',$IdModer->id)->where('active',null)->where('contact',null)->where('wait',null)->orderBy('estekdam_meli')->get();
            if($request->checkState=='full')
            $saveDarkhasts= Save_dakhast_esthdam::where('darkhast_id',$IdModer->id)->orderBy('estekdam_meli')->get();

            foreach( $saveDarkhasts as $r=>$saveDarkhast){
            $estekhdams[$r]=Estekhdam::where('meli_code',$saveDarkhast->estekdam_meli)->orderBy('meli_code')->first();
           }
           $originalLinks=[];
foreach ($estekhdams as $link){
    if(!in_array($link,$originalLinks)){
        array_push($originalLinks,$link);
    }
}
$c=$estekhdams;
$estekhdams =$originalLinks;
            $i=0;
            foreach( $estekhdams as $k=>$estekhdam){

                if($request->checkState==3)
                $sb=Save_dakhast_esthdam::where(['estekdam_meli'=>$estekhdam->meli_code,'darkhast_id'=>$IdModer->id,'active'=>3])->get();
                if($request->checkState==2)
                $sb=Save_dakhast_esthdam::where(['estekdam_meli'=>$estekhdam->meli_code,'darkhast_id'=>$IdModer->id,'contact'=>2])->get();
                if($request->checkState==1)
                $sb=Save_dakhast_esthdam::where(['estekdam_meli'=>$estekhdam->meli_code,'darkhast_id'=>$IdModer->id,'wait'=>1])->get();
                if($request->checkState==0)
                $sb=Save_dakhast_esthdam::where(['estekdam_meli'=>$estekhdam->meli_code,'darkhast_id'=>$IdModer->id])->where('active',null)->where('contact',null)->where('wait',null)->get();
                if($request->checkState=='full')
                $sb=Save_dakhast_esthdam::where(['estekdam_meli'=>$estekhdam->meli_code,'darkhast_id'=>$IdModer->id])->get();

                foreach( $sb as $s){
                $state[$i]=$s;
                $roles[$i]=RoleEstekhdam::find($state[$i]->role_id);
                $i++;
            }
        }
        $estekhdams =$c;
            return view('Estekhdami.vizit',compact('estekhdams','state','IdModer','roles'));
            return 'jkh';
        }
}
