<?php

namespace App\Http\Controllers;

use App\Models\ExitKala;
use App\Models\Kala;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class cashcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function cash()
    {

        $sum=[];
        $unit=[];
        $anbarId=User::where('username',Auth::user()->username)->first();
        $productsFname = Kala::where('repository_id',$anbarId->repository_id)->distinct()->get(['kala']);
        $fname_kalas=Kala::where('repository_id',Auth::user()->repository_id)->get();

        foreach($fname_kalas as $k=>$row){
        $q=Kala::where('kala', $row->kala)->first();
        $kala1= Kala::where(['kala' =>  $row->kala, 'repository_id' => Auth::user()->repository_id])->first();
        $exitkala_count = ExitKala::where('kala_id', $kala1->id)->sum('count_original');
        $sum[$k]= $q->products->sum('count_original')- $exitkala_count;
        $unit[$k]= $q->products()->first()->unit_product_title;
       

        }
        return view('products.cash', compact(  'productsFname','fname_kalas','sum','unit'));

    }

    public function cash_search(Request $request)
    {
        $sum=[];
        $unit=[];
        $anbarId=Kala::where('user_username',Auth::user()->username)->first();
        $productsFname = Kala::where('repository_id',$anbarId->id)->distinct()->get(['kala']); //search for select box name
        $fname_kalas=Kala::where('repository_id',Auth::user()->repository_id)->get();
        // start condition search
        foreach($fname_kalas as $k=>$row){
            // $q=Kala::where('kala', $row->kala)->first();
            $kala1= Kala::where(['kala' =>  $row->kala, 'repository_id' => Auth::user()->repository_id])->first();
            $exitkala_count = ExitKala::where('kala_id', $kala1->id)->sum('count_original');
            $sum[$k]=  $kala1->products->sum('count_original')- $exitkala_count;
            $unit[$k]=  $kala1->products()->first()->unit_product_title;
            }

        if ($request->fname != '---'){
            $kala1= Kala::where(['kala' => $request->fname, 'repository_id' => Auth::user()->repository_id])->first();
            $exitkala_count = ExitKala::where('kala_id', $kala1->id)->sum('count_original');
            $sum1[$k]= $kala1->products->sum('count_original')- $exitkala_count;
            $unit1[$k]=  $kala1->products()->first()->unit_product_title;
            return redirect()->to('/products/cash')->with('message','تعداد '.$sum1[$k]. '  ' .$unit1[$k]);
           }

           if ($request->count_original !=null){
            foreach($fname_kalas as $k=>$row){
                $kala1= Kala::where(['kala' =>  $row->kala, 'repository_id' => Auth::user()->repository_id])->first();
                $exitkala_count = ExitKala::where('kala_id', $kala1->id)->sum('count_original');
              if(($kala1->products->sum('count_original')- $exitkala_count)<$request->count_original ){
              $sum[$k]=$kala1->products->sum('count_original')- $exitkala_count;
                $unit[$k]='***';}
                }
           } //count

        return view('products.cash', compact(  'productsFname','fname_kalas','sum','unit'));
    }

    public function  code_searchCash(Request $request){
        $request->validate([
            'code' => 'required|numeric',
        ]);
        $fnameKala = Kala::where('id', $request->code)->first();
        if( $fnameKala==null)
        return redirect()->to('/products/cash')->with('message','کد موجود نمی باشد');
        $codekala=User::where(['repository_id'=>$fnameKala->repository_id,'username'=> Auth::user()->username])->first();
        if( $codekala==null)
        return redirect()->to('/products/cash')->with('message','کد موجود نمی باشد');
        $exitkala_count = ExitKala::where([ 'kala_id' => $fnameKala->id])->sum('count_original');
            $sum=$fnameKala->products->sum('count_original')- $exitkala_count;
              $unit=$fnameKala->products()->first()->unit_product_title;
              return redirect()->to('/products/cash')->with('message','تعداد '.$sum. '  ' .$unit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
