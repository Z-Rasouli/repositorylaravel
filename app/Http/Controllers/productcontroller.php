<?php

namespace App\Http\Controllers;

use App\Models\ExitKala;
use App\Models\Kala;
use App\Models\Product;
use App\Models\Unit_product;
use App\Models\User;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class productcontroller extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function kalaSearch(Request $request)
    {

        if ($request->date == '---' && $request->fname == '---')
            return redirect()->to('/products/')->with('message', 'چستجو براساس چی باشه!');
        //     if ($request->date != '---'){
        //         $v = verta($request->date);
        //     $f = Verta::getGregorian($v->year, $v->month, $v->day);
        //     $date1 = $f[0] . '-' . $f[1] . '-' . $f[2];
        // }
        $units = Unit_product::all();
        $kala = Kala::where(['repository_id' => Auth::user()->repository_id])->first();
        $productsFname = Product::where('user_username', Auth::user()->username)->distinct()->get(['fname']);
        $productsCreated = Product::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);
        // start convert date
        $a = [];
        $b = [];
        $i = 0;
        foreach ($productsCreated as $k => $row) {
            $v = verta($row->created_at);
            $f = Verta::getGregorian($v->year, $v->month, $v->day);
            $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
        }
        foreach ($a as $row) {
            if (!in_array($row, $b)) {
                $b[$i] = $row;
                $i++;
            }
        }
        $productsCreated = $b;
        // end conver date
        if ($request->date != '---' && $request->fname != '---')
            $products = Product::where(['user_username' => Auth::user()->username, 'fname' => $request->fname])->where('created_at', 'like',  $request->date . '%')->orderBy('created_at', 'desc')->paginate(5);
        elseif ($request->fname != '---')
            $products = Product::where(['user_username' => Auth::user()->username, 'fname' => $request->fname])->orderBy('created_at', 'desc')->paginate(5);
        else  //($request->date != '---')
            $products = Product::where('user_username', Auth::user()->username)->where('created_at', 'like',  $request->date . '%')->orderBy('created_at', 'desc')->paginate(5);

        return view('products.new', compact('units', 'products', 'productsFname', 'productsCreated', 'kala'));
    }


    public function search(Request $request)
    {

        $units = Unit_product::all();
        $products = Product::where('user_username', Auth::user()->username)->orderBy('created_at', 'desc')->paginate(5);
        $kala = Kala::where(['repository_id' => Auth::user()->repository_id, 'id' => $request->code])->first();
        $productsFname = Product::where('user_username', Auth::user()->username)->distinct()->get(['fname']);
        $productsCreated = Product::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);
        // start convert date
        $a = [];
        $b = [];
        $i = 0;
        foreach ($productsCreated as $k => $row) {
            $v = verta($row->created_at);
            $f = Verta::getGregorian($v->year, $v->month, $v->day);
            $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
        }
        foreach ($a as $row) {
            if (!in_array($row, $b)) {
                $b[$i] = $row;
                $i++;
            }
        }
        $productsCreated = $b;
        // end conver date

        if ($kala != null)
            return view('products.new', compact('units', 'products', 'productsFname', 'productsCreated', 'kala'));
        else {
            $kala = new Kala();
            $kala->kala = "یافت نشد";
            return view('products.new', compact('units', 'products', 'productsFname', 'productsCreated', 'kala'));
        }
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');

        $kala = new Kala();
        $kala->kala = "";
        $units = Unit_product::all();
        $products = Product::where('user_username', Auth::user()->username)->orderBy('created_at', 'desc')->paginate(5);
        $productsFname = Product::where('user_username', Auth::user()->username)->distinct()->get(['fname']);
        $productsCreated = Product::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);
        // start convert date
        $a = [];
        $b = [];
        $i = 0;
        foreach ($productsCreated as $k => $row) {
            $v = verta($row->created_at);
            $f = Verta::getGregorian($v->year, $v->month, $v->day);
            $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
        }
        foreach ($a as $row) {
            if (!in_array($row, $b)) {
                $b[$i] = $row;
                $i++;
            }
        }
        $productsCreated = $b;
        // end conver date
        return view('products.new', compact('units', 'products', 'productsFname', 'productsCreated', 'kala'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');

        $request->validate([
            'fname' => 'required',
            'count_original' => 'required|numeric',
        ]);
        $kala = new Kala();
        $kala->kala = "";
        $kala1 = Kala::where(['kala' => $request->fname, 'repository_id' => Auth::user()->repository_id])->first();
        if ($kala1 != null) {
            $product = new Product();
            $product->fname = $request->fname;
            $product->count_original = $request->count_original;
            $product->unit_product_title = $request->unit_product_title;
            $product->comment = $request->comment;
            $product->user_username = Auth::user()->username;
            $product->kala_id = $kala1->id;
            $product->save();
            $units = Unit_product::all();
            $products = Product::where('user_username', Auth::user()->username)->orderBy('created_at', 'desc')->paginate(5);
            $productsFname = Product::where('user_username', Auth::user()->username)->distinct()->get(['fname']);
            $productsCreated = Product::where('user_username', Auth::user()->username)->distinct()->get(['created_at']);
            // start convert date
            $a = [];
            $b = [];
            $i = 0;
            foreach ($productsCreated as $k => $row) {
                $v = verta($row->created_at);
                $f = Verta::getGregorian($v->year, $v->month, $v->day);
                $a[$k] = $f[0] . '-' . $f[1] . '-' . $f[2];
            }
            foreach ($a as $row) {
                if (!in_array($row, $b)) {
                    $b[$i] = $row;
                    $i++;
                }
            }
            $productsCreated = $b;
            // end conver date
            return view('products.new', compact('units', 'products', 'productsFname', 'productsCreated', 'kala'));
        }
        return redirect()->back()->withErrors('محصول در انبار معرفی نشده است');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');

        $kala = Product::where('id', $id)->first();

        return view('products.show_info', compact('kala'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');

        $kala = Product::where('id', $id)->first();
        $units = Unit_product::all();
        return view('products.new_product', compact('units', 'kala'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');

        $request->validate([
            'fname' => 'required',
            'count_original' => 'required|numeric',
        ]);
        $del = Product::find($request->code)->update([


            'count_original' => $request->count_original,
            'unit_product_title' => $request->unit_product_title,
            'comment' => $request->comment,

        ]);
        return redirect()->to('/products/buy/')->with('message', '  کالا' . '=>' . $request->fname . ' با موفقیت ویرایش شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');
        $del = Product::find($id);
        $cashproduct = Product::all()->sum('count_original');
        $cashExit = ExitKala::where('kala_id', $del->kala_id)->sum('count_original');
        $cashproduct = $cashproduct - $del->count_original;
        if ($cashproduct < $cashExit)
            return redirect()->to('/products/buy/')->with('message', 'این تعداد در انبار موجود نیست');
        else
            $del->delete();
        return redirect()->to('/products/buy/')->with('message', '  کالا با موفقیت حذف شد ');
    }

    public function SowNewKala(){
        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');
            $kalas = Kala::orderBy('id', 'desc')->paginate(5);
            return view('products.newKala',compact('kalas'));
     }

    public function savenewKala(Request $request){
        $access = ['مدیر'];
        $c = User::where('username', Auth::user()->username)->first();
        if (!in_array($c->roles()->first()->title, $access))
            abort(403, 'شما دسترسی به این صفحه ندارید');
// *********************
        $request->validate([
            'fname' => 'required',
        ]);

    $findkala=Kala::where('kala',$request->fname)->first();
    if ( $findkala!=null)
    return redirect()->to('/products/newKala')->with('message', 'این کالا در انبار موجود است');
    $new=new Kala();
    $new->kala=$request->fname;
    $new->repository_id=Auth::user()->repository_id;
    $new->user_username=Auth::user()->username;
    $new->save();
    $kalas = Kala::orderBy('id', 'desc')->paginate(5);

    echo redirect()->to('/products/newKala')->with('message', 'این کالا در انبار با موفقیت ثبت گردید ');
    return view('products.newKala',compact('kalas'));

}

}
