<?php

namespace App\Http\Controllers;

use App\Models\ExitKala;
use App\Models\Product;
use App\Models\Repository;
use App\Models\Role;
use App\Models\Unit_product;
use App\Models\User;
use App\Models\User_Role;
use Database\Seeders\RepositorySeed;
use Illuminate\Foundation\Auth\User as AuthUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class usercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function text(){


    // }
    public function index()
    {
        $access = ['مدیر'];
        $c=User::where('username',Auth::user()->username)->first();
            if (!in_array( $c->roles()->first()->title, $access))
                abort(403, 'شما دسترسی به این صفحه ندارید');
        $users = User::where('username', '<>', Auth::user()->username)->where('repository_id', Auth::user()->repository_id)->orderBy('created_at', 'desc')->paginate(5);
        return view('users.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $access = ['مدیر'];
        $c=User::where('username',Auth::user()->username)->first();
            if (!in_array( $c->roles()->first()->title, $access))
                abort(403, 'شما دسترسی به این صفحه ندارید');
        $roles = Role::all();
        $repositories = Repository::find(Auth::user()->repository_id);
        return view('users.new', compact('roles', 'repositories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'email|unique:users',
            'username' => 'required|digits:10',
            'password' => [
                'required',
                'string',
                'min:6',
                'required_with:password_confirmation',
                'same:password_confirmation',
                'regex:/[a-z]/',
                'regex:/[0-9]/',
            ],
            'password_confirmation' => 'min:6|max:16',
            'role' => 'required',
            'mobile' => 'required|regex:/(09)[0-9]{9}/',
        ]);
        $repository = Repository::where('title', $request->anbar)->first();
        if ($repository != null) {
            $anbar = $repository->id;
        }
        $user = User::where('username', $request->username)->first();
        if ($user != null) {
            if ($user->repository_id != $anbar)
                return redirect()->back()->withErrors('هر کاربر حق ثبت نام در یک انبار را دارد');
        }
        if ($user != null)
        return redirect()->back()->withErrors('صاحب نام کاربر در این انبار فعالیت میکند');


        $user = new User();
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->repository_id = $anbar;
        $user->save();


        foreach (Role::all()  as $role1) {

            if (in_array($role1->id, $request->get('role'))) {
                $role = new User_Role();
                $role->role_id =  $role1->id;
                $role->user_username = $request->username;
                $role->save();
            }
        }
        return redirect()->to('/users')->with('message', 'کاربر ' . $user->username . 'با موفقیت ذخیره شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $access = ['مدیر'];
        $c=User::where('username',Auth::user()->username)->first();
            if (!in_array( $c->roles()->first()->title, $access))
                abort(403, 'شما دسترسی به این صفحه ندارید');
        $checked = User::where('username', $user->username)->first();
        $checked = $checked->roles()->get();
        $roles = Role::all();
        return view('users.edit', compact('roles', 'checked', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'mobile' => 'required',
            'role' => 'required',
            'email' => 'required',
        ]);


        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->mobile = $request->mobile;
        $user->save();

        $user1 = User_Role::where('user_username', $user->username)->delete();
        foreach ($request->role as $role1) {
            $role = new User_Role();
            $role->role_id = $role1;
            $role->user_username = $user->username;
            $role->save();
        }
        return redirect()->to('/users')->with('message', 'کاربر' . $user->username . ' با موفقیت ویرایش شد');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        
        $access = ['مدیر'];
        $c=User::where('username',Auth::user()->username)->first();
            if (!in_array( $c->roles()->first()->title, $access))
                abort(403, 'شما دسترسی به این صفحه ندارید');
       $findproduct=Product::where('user_username',$user->username)->first();        
       $findExit=ExitKala::where('user_username',$user->username)->first(); 
       if( $findproduct!=null || $findExit!=null )    
       return redirect()->to('users')->with('message', $user->fname.' '.$user->lname.' به علت همکاری با مجموعه قابل حذف نیست' );
        $user->delete();
        return redirect()->to('users')->with('message', 'کاربر ' . $user->fname.' '.$user->lname . ' با موفقیت حذف شد');
    }
}
