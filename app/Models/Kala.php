<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kala extends Model
{
    use HasFactory;
    // public function users(){
    //     return $this->belongsToMany(User::class,'exit_kalas','kala_id','user_username');
    // }
    public function products(){
        return $this->hasMany(Product::class);
    }
}
