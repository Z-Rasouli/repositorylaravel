<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['count_original','unit_product_title','count_second','unit_second','comment'];
    public function kala(){
        return $this->belongsTo(Kala::class);
    }
}
