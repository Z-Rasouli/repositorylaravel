<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repository extends Model
{
    use HasFactory;
    protected $table="repositories";
    
    public function Users(){
        return $this->hasMany(User::class);
    }

}
