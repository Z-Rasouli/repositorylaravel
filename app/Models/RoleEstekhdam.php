<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleEstekhdam extends Model
{
    use HasFactory;
    public function Darkhast(){
        return $this->belongsTo(Darkhast::class);
    }
}
