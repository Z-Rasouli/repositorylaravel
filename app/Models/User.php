<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;

class User extends Authenticable
{
    use HasFactory;
    protected $primaryKey = "username";
    public $incrementing = false;
    
    public function Repository(){
        return $this->belongsTo(Repository::class);
    }
    public function roles(){
        return $this->belongsToMany(Role::class,'user_roles','user_username','role_id');
    }

    // public function kalas(){
    //     return $this->belongsToMany(Kala::class,'exit_kalas','user_username','kala_id');
    // }

}
