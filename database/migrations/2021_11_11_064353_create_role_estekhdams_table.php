<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleEstekhdamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_estekhdams', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedBigInteger('Darkhast_id');
            $table->foreign('Darkhast_id')->references('id')->on('Darkhasts')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->engine="InnoDb";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_estekhdams');
    }
}
