<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstekhdamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estekhdams', function (Blueprint $table) {
            $table->string('fname',20);
            $table->string('lname',25);
            $table->string('meli_code')->primary();
            $table->string('mobile',11);
            $table->text('Address')->nullable();
            $table->longText('comment')->nullable();
            $table->timestamps();
            $table->engine="InnoDb";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estekhdams');
    }
}
