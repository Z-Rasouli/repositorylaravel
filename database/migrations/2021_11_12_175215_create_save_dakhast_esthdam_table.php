<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaveDakhastEsthdamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('save_dakhast_esthdam', function (Blueprint $table) {

            $table->unsignedBigInteger('darkhast_id'); //field table darkhasts Summary Because Long
            $table->unsignedBigInteger('role_id');     //field table role_estekhdams Summary Because Long
            $table->string('estekdam_meli');           //field table estekhdams Summary Because Long
            $table->primary(['darkhast_id','role_id','estekdam_meli']);
            $table->foreign('darkhast_id')->references('id')->on('darkhasts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('role_id')->references('id')->on('role_estekhdams')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('estekdam_meli')->references('meli_code')->on('estekhdams')->onDelete('cascade')->onUpdate('cascade');
            $table->tinyInteger('wait')->nullable();
           $table->tinyInteger('contact')->nullable();
           $table->tinyInteger('active')->nullable();
            $table->timestamps();
            $table->engine="InnoDb";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('save_dakhast_esthdam');
    }
}
