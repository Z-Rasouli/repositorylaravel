<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExitKalasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exit_kalas', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('count_original');
            $table->string('unit_product_title');
            $table->text('comment')->nullable();
            $table->string('user_username');
            $table->foreign('user_username')->references('username')->on('users');
            $table->unsignedBigInteger('kala_id');
            $table->foreign('kala_id')->references('id')->on('kalas');
            $table->timestamps();
            $table->engine="InnoDb";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exit_kalas');
    }
}
