<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RepositorySeed::class,
            UserSeed::class,
            RoleSeed::class,
            UserRoleSeed ::class,
            Unit_productSeed ::class,
            KalaSeed::class,
            ProductSeed::class,
        ]);
    }
}
