<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KalaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kalas')->insert([
            'kala'=>'سیم جوش',
            'repository_id'=>'1',
            'user_username'=>'3330016647',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('kalas')->insert([
            'kala'=>'سنگ صفحه',
            'repository_id'=>'1',
            'user_username'=>'3330016647',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('kalas')->insert([
            'kala'=>'الکترود',
            'repository_id'=>'1',
            'user_username'=>'3330016647',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('kalas')->insert([
            'kala'=>'موتورجوش',
            'repository_id'=>'2',
            'user_username'=>'3330016648',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('kalas')->insert([
            'kala'=>'متر',
            'repository_id'=>'2',
            'user_username'=>'3330016648',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
    }
}
