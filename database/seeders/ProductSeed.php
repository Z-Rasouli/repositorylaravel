<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Products')->insert([
            'fname'=>'سیم جوش',
            'count_original'=>20,
            'unit_product_title'=>'بسته',
            'user_username'=>'3330016647',
            'kala_id'=>1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('Products')->insert([
            'fname'=>'سنگ صفحه',
            'count_original'=>50,
            'unit_product_title'=>'بسته',
            'user_username'=>'3330016647',
            'kala_id'=>2,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('Products')->insert([
            'fname'=>'الکترود',
            'count_original'=>50,
            'unit_product_title'=>'عدد',
            'user_username'=>'3330016647',
            'kala_id'=>3,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('Products')->insert([
            'fname'=>'موتورجوش',
            'count_original'=>50,
            'unit_product_title'=>'عدد',
            'user_username'=>'3330016648',
            'kala_id'=>4,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
    }
}
