<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RepositorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Repositories')->insert([
            'title'=>'ابزارالات',
            'address'=>'یزد',
            'mobile'=>'09388880874',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('Repositories')->insert([
            'title'=>'شیرالات',
            'address'=>'یزد',
            'mobile'=>'09388880414',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
    }
}
