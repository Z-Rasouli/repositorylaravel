<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Unit_productSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Unit_products')->insert([
            'title'=>'عدد',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('Unit_products')->insert([
            'title'=>'بسته',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('Unit_products')->insert([
            'title'=>'کارتون',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('Unit_products')->insert([
            'title'=>'کیلوگرم',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('Unit_products')->insert([
            'title'=>'گرم',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('Unit_products')->insert([
            'title'=>'گالن',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('Unit_products')->insert([
            'title'=>'لیتر',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('Unit_products')->insert([
            'title'=>'متر',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        
    }
}
