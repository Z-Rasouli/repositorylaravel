<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname'=>'محمد',
            'lname'=>'صابری',
            'username'=>'3330016647',
            'password'=>Hash::make('Aa1234'),
            'email'=>"zahra.rasouli.2018@gmail.com",
            'mobile'=>'09380987145',
            'repository_id'=>1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('users')->insert([
            'fname'=>'صادق',
            'lname'=>'کریمی',
            'username'=>'3330016648',
            'password'=>Hash::make('Aa1234'),
            'email'=>"moohamad.rasouli.2018@gmail.com",
            'mobile'=>'09380987144',
            'repository_id'=>2,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('users')->insert([
            'fname'=>'قباد',
            'lname'=>'کریمی',
            'username'=>'3330016649',
            'password'=>Hash::make('Aa1234'),
            'email'=>"moohamad.rasouli.2018@gmail.com",
            'mobile'=>'09380987144',
            'repository_id'=>1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
    }
}
