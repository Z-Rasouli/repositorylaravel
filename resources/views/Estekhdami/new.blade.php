<html>
    @include('layouts.head')
<body dir="rtl">

  <section class="d-flex justify-content-center mt-0 w-100 bg-dark" style="border-bottom: 5px solid rgb(221, 236, 9)" >
    <h5 class="my-3 text-white ">
فرم استخدامی انبار
</h5>
</section>
<br>
<div class="container m-auto">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">
<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>

<section class="container-fluid">
<div class="border border-warning p-4" >

<form method="post" action="/Estekhdami/save_user"  id="form_user" >
    @csrf

                    <section class="row">

                        <section class="col-12 col-md-6">
                            <div class="form-group text-right">
                                <label for="fname">نام<span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" id='fname' name="fname" value="{{old('fname')}}" autocomplete="off" placeholder="نام" autofocus>
                            </div>
                        </section>
                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="lname">نام خانوادگی<span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="lname" name="lname" value="{{old('lname')}}" autocomplete="off" placeholder="فامیلی">
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="meli_code"> کدملی <span class="text-danger">*</span> </label>
                                <input type="text" class="form-control form-control-sm"  id="meli_code" name="meli_code" value="{{old('meli_code')}}" autocomplete="off" >
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="mobile"> مبایل <span class="text-danger">*</span> </label>
                                <input type="text" class="form-control form-control-sm"  id="mobile" name="mobile" value="{{old('mobile')}}" autocomplete="off" placeholder="09380874586">
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="Address"> ادرس  </label>
                                <textarea class="form-control form-control-sm"  id="Address" name="Address"  autocomplete="off" >{{old('Address')}}</textarea>
                            </div>
                        </section>
                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="comment"> شرح سابقه کار</label>
                                <textarea class="form-control form-control-sm"  id="comment" name="comment"  autocomplete="off" rows="4" cols="60">{{old('comment')}}</textarea>
                            </div>
                        </section>
                        <section class="col-12">
                            <button type="submit" class="btn btn-primary btn-block"><b> ثبت نام</b></button>
                            <a href="#" onclick="change()" class="btn btn-primary btn-block"><b>قبلا ثبت نام کردم</b></a>
                        </section>

                        <div class="text-right m-3 ">
                       <i class="fas fa-hand-holding  fa-2x icon"></i>
                            <a href="#" onclick="change1()" class="text-danger font-weight-bold "> لیست انبارها و شرایط استخدام </a> </div> </section>
                         </div>
                    </form>

        </section>
    </section>
</section>
</div>
<section class="col-12  col-md-6  m-auto " id="meli" style="display:none">
    <section class="row">
  <section class="container-fluid ">
  <div class="border border-white p-4" >
   <section class="col-12 ">
    <form method="post" action="/Estekhdami/code_meli_befor" id="form_user_before"  >
        @csrf
        <section class="col-12 col-md-6">
            <div class="form-group  text-right">
                <label for="meli_code_r"> کدملی <span class="text-danger">*</span> </label>
                <input type="text" class="form-control form-control-sm"  id="meli_code_r" name="meli_code_r" value="{{old('meli_code_r')}}" autocomplete="off" >
            </div>
        </section>
        <section class="col-12">
            <button type="submit" class="btn btn-primary btn-block"><b>ورود</b></button>
        </section>
    </form>
   </section></div></section></section></section>

 {{--  create part anbar  --}}
 <section class="col-12  col-md-6  m-auto " id="demo" style="visibility: hidden">
  <section class="row">
<section class="container-fluid ">
<div class="border border-white p-4" >
 <section class="col-12 ">


 <table class="table table-bordered text-center font-weight-bold">
    <caption>استخدامی{{verta();}}</caption>
    <thead>
      <tr class=" text-black">
        <th scope="col">ردیف</th>
        <th scope="col">نام انبار</th>
        <th scope="col">آدرس</th>
        <th scope="col">شرایط استخدام</th>
        <th scope="col">سمت های مورد نیاز</th>
      </tr>
    </thead>
    <tbody>
        @foreach($AnbarNames as $k=>$AnbarName)
      <tr>
        <th scope="row">{{$k+1}}</th>
        <td >{{$AnbarName->title}}</td>
        <td>{{$AnbarName->address}}</td>
        <td> {{ $commentDarkhast[$k]}}</td>
        <td class="p-3">  @foreach ($roles[$k] as $ky=>$role)
        {{$role->title}}&nbsp;&nbsp;
        @endforeach
    </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>
</section>
</section>
</section>
<body>
    <script>
        function change1(){
            if( document.getElementById("demo").style.visibility=="visible")
            document.getElementById("demo").style.visibility="hidden";
            else
            document.getElementById("demo").style.visibility="visible";
        }

    </script>
    <script>
        function change(){
            if( document.getElementById("meli").style.display=="block")
            document.getElementById("meli").style.display="none";
            else
            document.getElementById("meli").style.display="block";
        }
    </script>
</html>

