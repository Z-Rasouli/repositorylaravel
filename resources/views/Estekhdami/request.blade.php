
@extends('layouts.master')
@section('pagetitle')
  {{--  پرسنل جدید  --}}
@endsection

@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item "> <a href="#">خانه</a></li>
      <li class="breadcrumb-item "> <a href="#">استخدام</a></li>
      <li class="breadcrumb-item  active" aria-current="page">  درخواست نیرو </li>
    </ol>
  </nav>
</div>
<br>
  <section class="d-flex justify-content-start  " style="margin-right: 150px"">
    <h5 id="demo">
         درخواست نیرو     </h5>

    <br><br>
</section>
<div class="container">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">

<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>



</form>

<form method="POST" action="/Estekhdami/saveRequest" >
    @csrf
                    <section class="row">

                        <section class="col-12">
                            <div class="form-group  text-right">
                                <label for="comment"> توضیحات  </label>
                                <textarea class="form-control form-control-sm " rows="4" cols="50"  id="comment" name="comment" autocomplete="off" placeholder="توضیحات">{{old('comment')}}</textarea>
                            </div>
                        </section>

                        <section class="col-3">
                            @foreach ($roles as $k=>$role)
                               <div class="form-check ">
                                   <input class="form-check-input" type="checkbox" name="role[]" id="role{{$k}}"  value="{{$role->id}}" @if(is_array(old('role')) && in_array($role->id, old('role'))) checked @endif>

                                   <label for="role{{$k}}" class="form-check-label mr-3 mt-1">{{$role->title}}</label>

                                   <label>

                               </div>
                           </section>
                           @endforeach

                        <section class="col-12">
                            <button class="btn btn-primary btn-sm">درخواست</button>
                        </section>

                </form>
        </section>
    </section>
{{-- </section> --}}
</div>


@endsection


