<html>
    @include('layouts.head')
<body dir="rtl">

  <section class="d-flex justify-content-center mt-0 w-100 bg-dark    " style="border-bottom: 5px solid rgb(221, 236, 9)" >
    <h5 class="my-3 text-white ">
فرم استخدامی انبار
</h5>
</section>
<br>
<div class="container m-auto">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">
<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start "><b>{{$errors->first()}}</b></div>
    @endif
</section>

    </section>
</section>
</div>

 {{--  create part anbar  --}}
 <section class="col-12  col-md-6  m-auto">
  <section class="row">
<section class="container-fluid ">
<div class="border border-white p-4" >
 <section class="col-12 ">
     <div class=" text-right "><b >ادامه ی ثبت نام&nbsp;&nbsp;{{$person->fname}}&nbsp;{{$person->lname}}</b></div>


                        <table class="table table-bordered table-hover text-center font-weight-bold">
                            <caption class="text-right mt-2 text-success">استخدامی{{verta();}}</caption>
                            <thead>
                              <tr class=" text-black table-dark">
                                <th scope="col">ردیف</th>
                                <th scope="col">نام انبار</th>
                                <th scope="col">آدرس</th>
                                <th scope="col">شرایط استخدام</th>
                                <th scope="col">سمت های مورد نیاز</th>
                                <th scope="col">ارسال درخواست</th>
                                <th scope="col">ویرایش درخواست</th>
                                <th scope="col">حذف درخواست</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($AnbarNames as $k=>$AnbarName)
                                <form method="POST" action="/Estekhdami/save_formEstakhdami" >
                                    @csrf
                                    <input type="hidden" value="{{$person->meli_code}}" id="person" name="person"/>
                                    <input type="hidden" value="{{$AnbarName->title}}" id="AnbarName" name="AnbarName"/>
                                    <input type="hidden" value="{{$darkhastUsername[$k]}}" id="darkhastUsername" name="darkhastUsername"/>
                              <tr>
                                <th scope="row">{{$k+1}}</th>
                                <td >{{$AnbarName->title}}</td>
                                <td>{{$AnbarName->address}}</td>
                                <td> {{ $commentDarkhast[$k]}}</td>
                                <td class="p-3" >  @foreach ($roles[$k] as $ky=>$role)
                                    <div class="form-check ">
                                    <input class="form-check-input " type="checkbox" name="role[]" id="role{{$k}}"  value="{{$role->id}}" @if(is_array(old('role')) && in_array($role->id, old('role'))) checked @endif>
                                    <label for="role{{$k}}" class="form-check-label mr-3 ">{{$role->title}}</label>
                                    </div>
                                    @endforeach
                            </td>
                            <td ><button type="submit" class="btn btn-primary btn-block"><b>ارسال</b></button>
                            </td>
                            <td><a href="#"><i class="fa fa-edit fa-2x text-primary" title="ویرایش"></i> </a>
                            </td>
                            <td><a href="#" ><i class="fa fa-trash-alt fa-2x  text-danger" title="حذف"></i> </a>
                            </td>
                              </tr>
                                </form>
                              @endforeach
                            </tbody>
                          </table>


</div>

</section>
</section>
</section>

<body>

</html>

