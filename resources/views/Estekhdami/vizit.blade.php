
@extends('layouts.master')
@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item "> <a href="#">خانه</a></li>
      <li class="breadcrumb-item "> <a href="#">استخدامی</a></li>
      <li class="breadcrumb-item  active" aria-current="page">  بازدید تقاضا
  </nav>
</div>
<br>
  <section class="d-flex justify-content-start  " style="margin-right: 150px"">
    <h5 id="demo">
        بازدید تقاضا      </h5>
    <br><br>
</section>
<div class="container">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">
<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>

<section class="container-fluid">

    <form method="" action="" >@csrf
        <div class="input-group">
            <input type="text" class="form-control form-control-sm" placeholder="" name='code' autocomplete="off" value="{{old('code')}}">
            <button type="submit" class="btn btn-warning search-button mr-2">
              <i class="fas fa-search text-info"></i>
            </button>
          </div>
        </section>
</form>

        </section>
    </section>
</section>
</div>

<div class="container ">
    <div class="row">
        <div class="col-12  col-md-9  m-auto">

  <div class="d-flex justify-content-start col-3">
     <form method="post" action="/Estekhdami/vizit/searchState" >@csrf
        <fieldset  class="border pr-3 pl-3 mb-2">
            <legend class="text-right w-auto">جستجو:</legend>

            <div class="form-group  text-right  font-weight-bold">
                <label for="date" class="text-success"> وضعیت </label>

                <select name="checkState" id="checkState" class="form-control form-control-sm " >
                 <option value="full">همه</option>
                    <option value="3"  >فعال </option>
                    <option value="2"  >تماس </option>
                    <option value="1"  >در صف انتظار </option>
                    <option value="0"  >خنثی</option>

             </select>
            </div>
       <input type="submit" value="جستجو" >
    </fieldset>
    </form>
          </div>
    <br>
    <div class="d-flex justify-content-center text-success font-weight-bold">
    <span>لیست افرادی که در خواست استخدام دارند با مدیریت  {{ Auth::user()->fname}} {{ Auth::user()->lname}}</span>
</div>
    <table class="table table-striped table-bordered table-hover "style="font-size: .8rem">
  <thead>

    <tr class="table-primary text-center">
        <th scope="col" class="text-center">ردیف</th>
        <th scope="col" style="width:15%" >نام و نام خانوادگی</>
      <th scope="col" class="text-center">کد ملی</th>
      <th scope="col" class="text-center">شماره مبایل </th>
      <th scope="col" class="text-center">سمت</th>
      <th scope="col" class="text-center" style="width: 80%">توضیحات</th>
      <th scope="col" class="text-center">درصف انتظار</th>
      <th scope="col" class="text-center" >تماس</th>
      <th scope="col" class="text-center" >فعال</th>
      <th scope="col" class="text-center"  style="width: 15%"><i class="fa fa-cogs"></i> تنظیمات</th>
    </tr>
  </thead>
  <tbody>
    @php($key=1)
    @foreach($estekhdams as $k=>$estekhdam)
    <form method="post" action="/Estekhdami/saveVizit" >@csrf
        <input type="hidden" name="meli" value="{{$estekhdam->meli_code}}"/>
        <input type="hidden" name="IdModer" value="{{$IdModer->id}}"/>
        <input type="hidden" name="roleperson" value="{{$roles[$k]->id}}"/>
    <tr class="text-center">
      <td class="table-primary">{{ $key}}</td>
      <td>{{$estekhdam->fname}}&nbsp;{{$estekhdam->lname}}</td>
      <td >{{$estekhdam->meli_code}}</td>
      <td>{{$estekhdam->mobile}}</td>
      <td>{{$roles[$k]->title}}</td>
      <td>{{$estekhdam->comment}}</td>
      <td> <input type="radio" name="state[]" id="state{{$k}}" value="1" @if($state[$k]->wait==1)) checked   @endif></td>
      <td> <input type="radio" name="state[]" id="state{{$k}}" value="2" @if($state[$k]->contact==2)) checked @endif></td>
      <td> <input type="radio" name="state[]" id="state{{$k}}" value="3" @if($state[$k]->active==3)) checked @endif></td>
       <td>
           <button type="submit" ><i class="fas fa-save  text-info" title="ثبت"></i></button>
 <a href="#"><i class="fa fa-trash-alt text-danger" title="حذف"></i> </a>
        </td>
    </tr>
    @php($key++)
    </form>
    @endforeach
  </tbody>
</table>
</div>
</div>
</div>
@endsection


