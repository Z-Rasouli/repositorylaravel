<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body >
    <br>
    <br>
    <br>
  
    <div class="container  mt-3">
            <div class="col-xs-1" align="center">
                <div class="row">
                    <div class="col-md-4  m-auto">
                        <div class="panel panel-default">
                            <div class="panel-heading">



                                <h3 class="panel-title mb-4 text-info">ورود به انبار کالا</h3>
                            </div>
                            <br>
                            <div class="panel-body">
                                <form accept-charset="UTF-8" role="form" method="post" action="{{route('login')}}">
                                    @csrf
                                    <fieldset >

                                        <div class="form-group">
                                            <input class="form-control text-center  rounded-pill border-success  @error('username') is-invalid @enderror" placeholder="نام کاربری" name="username" type="text" autocomplete="off">
                                            @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control text-center rounded-pill border-success @error('password') is-invalid @enderror" placeholder="گذرواژه" name="password" type="password" autocomplete="off">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <input class="btn btn-lg btn-info btn-block  rounded-pill" type="submit" value="ورود">
                                        <a href="Estekhdami/requisition_person" class="btn btn-lg btn-info btn-block  rounded-pill" >استخدامی</a>
                                    </fieldset>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</body>

</html>
