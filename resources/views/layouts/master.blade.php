
<!DOCTYPE html>
<html lang="en" dir="rtl">

@include('layouts.head')

<body >
 @include('layouts.sidebar')
@include('layouts.topnav')
  <main>
    <!-- card -->
    <div class="row mx-0">
      <div class="col-xl-10 col-lg-9 col-md-8 mr-auto">
        <div class="row pt-md-5 mt-md-3 mb-5 mx-0">
          <!-- sales -->
          <div class="col-xl-3 col-sm-6  p-2">
            <div class="card border-0 card-common">
              <div class="card-body">
                <div class="d-flex justify-content-between">
                  <i class="fas fa-shopping-cart fa-3x text-warning"></i>
                  <div class=" text-right text-secondary">
                    <h5>تعداد انبار</h5>
                    <h3 class="text-md-small">2</h3>
                  </div>
                </div>
              </div>
              <div class="card-foorter text-right mr-3 text-secondary">

              </div>
            </div>
          </div>
          <!-- Money -->

          <!-- user -->
          <div class="col-xl-3 col-sm-6 p-2">
            <div class="card border-0 card-common">
              <div class="card-body">
                <div class="d-flex justify-content-between">
                  <i class="fas fa-user fa-3x text-info"></i>
                  <div class=" text-right text-secondary">
                    <h5>تعداد کاربران</h5>
                    <h3 class="text-md-small">5</h3>
                  </div>
                </div>
              </div>
              <div class="card-foorter text-right mr-3 text-secondary">

              </div>
            </div>
          </div>
          <!-- chart -->
          <div class="col-xl-3 col-sm-6 p-2">
            <div class="card border-0 card-common">
              <div class="card-body">
                <div class="d-flex justify-content-between">
                  <i class="fas fa-chart-line fa-3x text-danger"></i>
                  <div class=" text-right text-secondary">
                    <h5>تعداد تقاضا</h5>
                    <h3 class="text-md-small">12</h3>
                  </div>
                </div>
              </div>
              <div class="card-foorter text-right mr-3 text-secondary">

              </div>
            </div>
          </div>

          <div class="col-xl-3 col-sm-6 p-2">
            <div class="card border-0 card-common">
              <div class="card-body">
                <div class="d-flex justify-content-between">
                  <i class="fas fa-money-bill fa-3x text-success"></i>
                  <div class=" text-right text-secondary">
                    <h5>ساعت</h5>
                    <h3 class="text-md-small">{{date("H:i:s"); }}</h3>
                  </div>
                </div>
              </div>
              <div class="card-foorter text-right mr-3 text-secondary">

              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- end of card -->
<!--start my main-->
<div class='container'>
    <h3 class="mb-0">@yield('pagetitle')</h3>

    @if(session()->has('message'))
    <div class="alert alert-success text-center m-auto w-75">
        {{ session()->get('message') }}
    </div>
    @endif

    <br>
    @yield('content')
</div>
  </main>
    <!-- footer -->
   {{-- @include('layouts.footer') --}}
    <!-- end of footer -->
    <script src="{{mix('js/admin.js')}}"></script>
</body>

</html>
