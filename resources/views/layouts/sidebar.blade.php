<!-- navbar -->
<nav class="navbar navbar-expand-md navbar-light">
  <button class="navbar-toggler mb-2 bg-light" type="button" data-toggle="collapse" data-target="#navbarNav"
    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <div class="container-fluid">
      <div class="row">
        <!-- sidebar -->
        <div class="col-xl-2 col-lg-3 col-md-4 sidebar fixed-top">
          <p href="#" class="navbar-brand text-white d-block mx-auto text-center py-3 mb-4 bottom-border">
            خوش امدید</p>
          <div class="d-flex justify-content-center align-self-center bottom-border pb-2">
            <img src="/img/logo.jpg" alt="" width="40px" height="30px" class="rounded-circle ml-2">
            <span class="text-white"><a href="#" onclick="myFunction()" class="text-decoration-none font-weight-bold ">سلام &nbsp;{{Auth::user()->fname}} {{Auth::user()->lname}}</a></span>
            <script>
                function myFunction() {
                    alert(" خوبی؟");
                }
                </script>
        </div>
          <ul class="navbar-nav flex-column text-right mt-2 font-weight-bold " style="font-size:1.5rem ">
            <li class="nav-item pl-3">
              <a href="/home" class="nav-link text-white p-2 mb-2 current font-weight-normal "><i
                  class="fas fa-home text-light fa-sm ml-3"></i>خانه</a>
            </li>

            <li class="nav-item pl-3">
              <a href="#" class="nav-link text-white p-2 mb-2 sidebar-link"><i
                  class="fas fa-user text-light fa-lg ml-3"></i>پروفایل</a>
            </li>


            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-user  icon"></i>
                    <span>مدیریت کاربران</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="/users">لیست کاربران</a>
                    <a href="/users/create">پرسنل جدید</a>
                </section>
            </section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-file-alt text-light icon"></i>
                    <span>مدیریت انبار</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href=""> انبار جدید</a>
                    <a href="">نقش های انبار </a>
                    <a href="">لیست انبارها</a>
                </section>
            </section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-table text-light  icon"></i>
                    <span>مدیریت کالا</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="/products/newKala"> کالای جدید</a>
                    <a href="#">لیست کالاها</a>
                    <a href="/products/buy">فاکتور خرید</a>
                    <a href="/products/exit">خروج کالا</a>
                    <a href="/products/cash">موجودی کالاها </a>
                    <a href="#">برگشت خروج از انبار</a>
                </section>
            </section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-table text-light  icon"></i>
                    <span>مدیریت واحدکالا</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="">لیست واحدها</a>
                    <a href="">واحد جدید</a>
                    <a href="">ویرایش/حذف واحد </a>

                </section>
            </section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-hand-holding  fa-2x icon"></i>
                    <span>استخدامی</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="/Estekhdami/request">درخواست نیرو</a>
                    <a href="#">لیست درخواست نیرو</a>
                    <a href="/Estekhdami/vizit">بازدید تقاضا</a>
                </section>
            </section>

          </ul>
        </div>
        <!--end of sidbar -->
