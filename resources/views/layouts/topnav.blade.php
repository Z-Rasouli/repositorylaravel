<header>

            <!-- top nav -->
            <div class="col-xl-10 col-lg-9 col-md-8 mr-auto bg-dark fixed-top py-2 top-navbar">
              <div class="row align-items-center">
                <div class="col-md-3 text-right mb-0">
                  {{--  <h4 class="text-light">داشبورد</h4>  --}}
                </div>
                <div class="col-md-6">
                  <form>
                    <div class="input-group">
                      <input type="text" class="form-control search-input" placeholder="جستجو....">
                      <button type="button" class="btn btn-light search-button">
                        <i class="fas fa-search text-danger"></i>
                      </button>
                    </div>
                  </form>
                </div>
                <div class="col-md-3">
                  <ul class="navbar-nav">
                    {{-- <li class="nav-item icon-parent">
                      <a href="#" class="nav-link">
                        <i class="fas fa-comment text-muted fa-lg"></i>
                      </a>
                    </li>
                    <li class="nav-item icon-parent">
                      <a href="#" class="nav-link">
                        <i class="fas fa-bell text-muted fa-lg "></i>
                      </a>
                    </li> --}}
                    <li class="nav-item">
                      <a href="#" class="nav-link" data-toggle="modal" data-target="#sing-out" title="خروج" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt text-danger fa-lg mr-auto"></i>
                      </a>
                      <form action="{{route('logout')}}" id="logout-form" method="post">@csrf</form>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- end of top nav -->
          </div>
        </div>
      </div>
    </nav>
    <!-- end of navbar -->
  </header>
