
@extends('layouts.master')
@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item "> <a href="#">خانه</a></li>
      <li class="breadcrumb-item "> <a href="#">مدیریت کالا</a></li>
      <li class="breadcrumb-item  active" aria-current="page">  موجودی کالا
  </nav>
</div>
<br>
  <section class="d-flex justify-content-start  " style="margin-right: 150px"">
    <h5 id="demo">
    موجودی کالا     </h5>
    <br><br>
</section>
<div class="container">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">
<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>

<section class="container-fluid">

    <form method="post" action="/products/code_searchCash" >@csrf
        <div class="input-group">
            <input type="text" class="form-control form-control-sm" placeholder="کد" name='code' autocomplete="off" value="{{old('code')}}">
            <button type="submit" class="btn btn-warning search-button mr-2">
              <i class="fas fa-search text-info"></i>
            </button>
          </div>
        </section>

</form>
    </section>
</section>
</div>

<div class="container ">
    <div class="row">
        <div class="col-12  col-md-9  m-auto">

  <div class="d-flex justify-content-start col-3">
    <form method="post" action="{{route('cash_search')}}" >@csrf

            <div class="form-group  text-right">
                <label for=""> نام</label>
                <select name="fname" id="fname" class="form-control form-control-sm ">
                    <option value="---">---</option>
                    @foreach($productsFname as $productFname)
                    <option value="{{$productFname->kala}}"  @if (old('fname') == $productFname->kala) selected="selected" @endif>{{$productFname->kala}}</option>
                    @endforeach
             </select>
            </div>
            <section class="col-12 ">
                <div class="form-group  text-right">
                    <label for="count_original">تعداد کمتر از </label>
                    <input type="number" class="form-control form-control-sm " id="count_original" name="count_original"  autocomplete="off" placeholder="تعداد">
                </div>
            </section>


       <input type="submit" value="جستجو" >

    </form>
          </div>
    <br>
    <div class="d-flex justify-content-center text-success font-weight-bold">
    <span>لیست موجودی در انبار   {{ Auth::user()->Repository()->first()->title}} </span>
</div>
    <table class="table table-striped  table-hover "style="font-size: .8rem">
  <thead>

    <tr class="table-primary text-center">
        <th scope="col" class="text-center">ردیف</th>
      <th scope="col" class="text-center">کد</th>
      <th scope="col" style="width:25%" >نام</>
      <th scope="col" class="text-center">تعداد </th>
      <th scope="col" class="text-center">واحد اصلی</th>
    </tr>
  </thead>
  <tbody>
    @php($key1=1)
    @foreach($fname_kalas as $key=>$row)
    @if($unit[$key]=='***')
    <tr class="text-center table-danger">
      <td class="table-primary">{{ $key1}}</td>
      <td >{{$row->id}}</td>
      <td>{{$row->kala}}</td>
      <td>{{$sum[$key]}}</td>
      <td>{{$unit[$key]}}</td>
    </tr>
@else
    <tr class="text-center">
      <td class="table-primary">{{ $key1}}</td>
      <td >{{$row->id}}</td>
      <td>{{$row->kala}}</td>
      <td>{{$sum[$key]}}</td>
      <td>{{$unit[$key]}}</td>
    </tr>
    @endif
    @php($key1++)
    @endforeach
  </tbody>
</table>
</div>
</div>
</div>
@endsection


