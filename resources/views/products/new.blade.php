
@extends('layouts.master')
@section('pagetitle')
  {{--  پرسنل جدید  --}}
@endsection

@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item "> <a href="#">خانه</a></li>
      <li class="breadcrumb-item "> <a href="#">مدیریت کالا</a></li>
      <li class="breadcrumb-item  active" aria-current="page">  فاکتور خرید</li>
    </ol>
  </nav>
</div>
<br>
  <section class="d-flex justify-content-start  " style="margin-right: 150px"">
    <h5 id="demo">
        فاکتور خرید     </h5>
 <script>
    document.getElementById("demo").onmouseover = function() {mouseOver()};
    document.getElementById("demo").onmouseout = function() {mouseOut()};

    function mouseOver() {
      document.getElementById("demo").style.color = "red";
    }

    function mouseOut() {
      document.getElementById("demo").style.color = "black";
    }
    </script>
    <br><br>
</section>
<div class="container">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">

<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>

<section class="container-fluid">

    <form method="post" action="/products/search" >@csrf
        <div class="input-group">
            <input type="text" class="form-control form-control-sm" placeholder="کد" name='code' autocomplete="off" value="{{old('code')}}">
            <button type="submit" class="btn btn-warning search-button mr-2">
              <i class="fas fa-search text-info"></i>
            </button>
          </div>
        </section>

</form>

<form method="POST" action="/products/save" >
    @csrf
                    <section class="row">


                        <section class="col-12 ">
                            <div class="form-group text-right  ">
                                <label for="fname">نام<span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm  text-danger" id='fname' name="fname" value="{{$kala->kala}}" autocomplete="off" placeholder="نام کالا" >
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="count_original">تعداد <span class="text-danger">*</span></label>
                                <input type="number" class="form-control form-control-sm" id="count_original" name="count_original" value="{{old('count_original')}}" autocomplete="off" placeholder="تعداد">
                            </div>
                        </section>


                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="">واحد اصلی </label>
                                <select name="unit_product_title" id="unit_product_title" class="form-control form-control-sm">
                                    @foreach ($units as $k=>$unit)
                                    <option value="{{$unit->title}}"  @if (old('unit_product_title') == $unit->title) selected="selected" @endif>{{$unit->title}}</option>
                                    @endforeach
                             </select>
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="comment"> توضیحات  </label>
                                <textarea class="form-control form-control-sm " rows="4" cols="50"  id="comment" name="comment" autocomplete="off" placeholder="توضیحات">{{old('comment')}}</textarea>

                            </div>
                        <section class="col-12">
                            <button class="btn btn-primary btn-sm">ثبت</button>
                        </section>
                    </section>
                </form>
        </section>
    </section>
</section>
</div>

<div class="container ">
    <div class="row">
        <div class="col-12  col-md-9  m-auto">

  <div class="d-flex justify-content-start col-3">
    <form method="post" action="/products/kalaSearch" >@csrf
        <fieldset  class="border pr-3 pl-3 mb-2">
            <legend class="text-right w-auto">جستجو:</legend>
            <div class="form-group  text-right">
                <label for=""> نام</label>
                <select name="fname" id="fname" class="form-control form-control-sm ">
                    <option value="---">---</option>
                    @foreach($productsFname as $product)
                    <option value="{{$product->fname}}"  @if (old('fname') == $product->fname) selected="selected" @endif>{{$product->fname}}</option>
                    @endforeach
             </select>
            </div>

            <div class="form-group  text-right ">
                <label for=""> تاریخ ثبت</label>

                <select name="date" id="date" class="form-control form-control-sm " >
                 <option value="---">---</option>
                 @foreach($productsCreated as $product)
                    <option value="{{$product}}"  @if (old('date') == $product) selected="selected" @endif> {{(new Verta($product))->format('Y/n/j')}} </option>
                    @endforeach
             </select>
            </div>
       <input type="submit" value="جستجو" >
    </fieldset>
    </form>
          </div>
    <br>
    <div class="d-flex justify-content-center text-success font-weight-bold">
    <span>لیست اقلام ثبت شده ی توسط {{ Auth::user()->fname}} {{ Auth::user()->lname}}</span>
</div>
    <table class="table table-striped  table-hover "style="font-size: .8rem">
  <thead>

    <tr class="table-primary text-center">
        <th scope="col" class="text-center">ردیف</th>
      <th scope="col" class="text-center">کد</th>
      <th scope="col" style="width:25%" >نام</th>
      <th scope="col" class="text-center">تعداد </th>
      <th scope="col" class="text-center">واحد اصلی</th>
      <th scope="col" class="text-center"style="width: 25%">توضیح</th>
      <th scope="col" class="text-center" style="width: 15%">تاریخ و زمان ایجاد</th>
      <th scope="col" class="text-center" style="width: 15%">تاریخ و زمان ویرایش</th>
      <th scope="col" class="text-center"><i class="fa fa-cogs"></i> تنظیمات</th>
    </tr>
  </thead>
  <tbody>
    @php($key=0)
    @foreach($products as $product)
    <tr class="text-center">
      <td class="table-primary">{{$products->firstItem() + $key}}</td>
      <td >{{$product->kala_id}}</td>
      <td>{{$product->fname}}</td>
      <td>{{$product->count_original}}</td>
      <td>{{$product->unit_product_title}}</td>
      <td>{{$product->comment}}</td>
     <td>{{(new Verta($product->created_at))->format('Y/n/j H:i')}}</td>
     <td>{{(new Verta($product->updated_at))->format('Y/n/j H:i')}}</td>



       <td>
 <a href="/products/buy/{{$product->id}}/edit" ><i class="fa fa-edit text-info" title="ویرایش"></i> </a>|
 <a href="/products/buy/{{$product->id}}/info_del"><i class="fa fa-trash-alt text-danger" title="حذف"></i> </a>


        {{--  <a href="/products/buy/{{$product->username}}" onclick="event.preventDefault(); document.getElementById('form-delete-{{$user->username}}').submit();">حذف</a>
        <form action="/products/buy/{{$product->username}}" id="form-delete-{{$user->username}}" method="post">
          @method('DELETE')
          @csrf</form>  --}}

        </td>
    </tr>
    @php($key++)
    @endforeach
  </tbody>
</table>
<div class="d-flex justify-content-center">{!! $products->links( "pagination::bootstrap-4") !!}</div>
</div>

</div>
</div>
@endsection


