@extends('layouts.master')
@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
    <nav aria-label="breadcrumb" >
        <ol class="breadcrumb ">
          <li class="breadcrumb-item "> <a href="#">خانه</a></li>
          <li class="breadcrumb-item "> <a href="#">مدیریت کالا </a></li>
          <li class="breadcrumb-item  active" aria-current="page">  کالای جدید  </li>
        </ol>
      </nav>
    </div>
    <br>
      <section class="d-flex justify-content-start  " style="margin-right: 150px"">
        <h5>
            کالای جدید
        </h5>
        <br><br>
    </section>

    <div class="container">
        <section class="row">
          <section class="col-12  col-md-6  m-auto ">

            @if($errors->any())
<div class="alert alert-danger text-right">{{$errors->first()}}</div>
@endif

<form method="post" action="/products/savenewKala"  >
    @csrf

    <div class="form-group row">
        <label for="fname" class="col-sm-2 col-form-label">نام</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="fname" name="fname" value="{{old('fname')}}" autocomplete="off" autofocus>
        </div>
    </div>



    <div class="d-flex justify-content-start " style="margin:5%  20% 0 0">
    <button type="submit" class="btn btn-primary ">ثبت</button>
</div>
</form>
</section>
</section>
</div>

<div class="container ">
    <div class="row">
        <div class="col-12  col-md-9  m-auto">

  <div class="d-flex justify-content-start col-3">
    <form method="" action="" >
        @csrf

            <section class="col-12 ">
                <div class="form-group  text-right">
                    <label for="fname">  نام کالا </label>
                    <input type="text" class="form-control form-control-sm " id="fname" name="fname"  autocomplete="off" placeholder="نام کالا">
                </div>
            </section>


       <input type="submit" value="جستجو" >

    </form>
          </div>
    <br>
    <div class="d-flex justify-content-center text-success font-weight-bold">
    <span>لیست کالاهای انبار   {{ Auth::user()->Repository()->first()->title}} </span>
</div>
    <table class="table table-striped  table-hover "style="font-size: .8rem">
  <thead>

    <tr class="table-primary text-center">
        <th scope="col" class="text-center">ردیف</th>
      <th scope="col" class="text-center">کد</th>
      <th scope="col" style="width:25%" >نام</>
      <th scope="col" style="width:25%" >تنظیمات</>

    </tr>
  </thead>
  <tbody>
    @php($key1=1)
    @foreach ($kalas as $row)
    <tr class="text-center table-danger">
      <td class="table-primary">{{ $key1}}</td>
      <td >{{$row->id}}</td>
      <td>{{$row->kala}}</td>
      <td>
      <a href="#"><i class="fa fa-edit text-info" title="ویرایش"></i> </a>|
      <a href="#"><i class="fa fa-trash-alt text-danger" title="حذف"></i> </a>
    </td>
    </tr>
    @php($key1++)
    @endforeach
  </tbody>
</table>
<div class="d-flex justify-content-center">{!! $kalas->links( "pagination::bootstrap-4") !!}</div>

</div>
</div>
</div>
@endsection
