@extends('layouts.master')
@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item "> <a href="#">خانه</a></li>
      <li class="breadcrumb-item "> <a href="#">مدیریت کالا</a></li>
      <li class="breadcrumb-item "> <a href="#">  فاکتور خرید</a></li>
      <li class="breadcrumb-item  active" aria-current="page"> ویرایش کالا</li>
    </ol>
  </nav>
</div>
<br>
  <section class="d-flex justify-content-start  " style="margin-right: 150px"">
    <h5 id="demo">
        ویرایش کالا      </h5>

    <br><br>
</section>
<div class="container">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">

<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>

<section class="container-fluid">



<form method="POST" action="/products/buy/update" >
    @csrf

                            <div class="form-group text-right  ">
                                <input type="hidden" class="form-control form-control-sm  text-danger" id='code' name="code" value="{{$kala->id}}" autocomplete="off" placeholder="نام کالا" >
                            </div>


                        <section class="col-12 ">
                            <div class="form-group text-right  ">
                                <label for="fname">نام<span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm  text-danger" readonly id='fname' name="fname" value="{{$kala->fname}}" autocomplete="off" placeholder="نام کالا" >
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="count_original">تعداد <span class="text-danger">*</span></label>
                                <input type="number" class="form-control form-control-sm" id="count_original" name="count_original" value="{{$kala->count_original}}" autocomplete="off" placeholder="تعداد">
                            </div>
                        </section>


                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="">واحد اصلی </label>
                                <select name="unit_product_title" id="unit_product_title" class="form-control form-control-sm">
                                    @foreach ($units as $k=>$unit)
                                    <option value="{{$unit->title}}"  @if (old('unit_product_title') == $unit->title) selected="selected" @endif>{{$unit->title}}</option>
                                    @endforeach
                             </select>
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="comment"> توضیحات  </label>
                                <textarea class="form-control form-control-sm " rows="4" cols="50"  id="comment" name="comment" autocomplete="off" placeholder="توضیحات">{{$kala->comment}}</textarea>

                            </div>
                        <section class="col-12">
                            <button class="btn btn-primary btn-sm">ویرایش</button>
                            <a href="/products/buy" class="btn btn-primary btn-sm ml-5">کنسل</a>
                        </section>
                    </section>
                </form>
        </section>
    </section>
</section>
</div>
@endsection
