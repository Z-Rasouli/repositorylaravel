@extends('layouts.master')
@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item "> <a href="#">خانه</a></li>
      <li class="breadcrumb-item "> <a href="#">مدیریت کالا</a></li>
      <li class="breadcrumb-item "> <a href="#">  فاکتور خرید</a></li>
      <li class="breadcrumb-item  active" aria-current="page"> حذف کالا</li>
    </ol>
  </nav>
</div>
<br>
  <section class="d-flex justify-content-start  " style="margin-right: 150px"">
    <h5 id="demo">
        حذف کالا      </h5>

    <br><br>
</section>
<div class="container">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">

<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>

<section class="container-fluid  m-auto">
<table class="table table-hover">

        <tr class="table-primary" >
            <th style="width: 5%">نام</th>
            <td >{{$kala->fname}}</td>
        </tr>
        <tr class="table-success">
            <th>تعداد اصلی</th>
            <td>{{$kala->count_original}}{{$kala->unit_product_title}}</td>
        </tr>
        <tr class="table-info">
            <th> توضیحات</th>
            <td>{{$kala->comment}}</td>
        </tr>

</table>
<br>
<a href="/products/buy/{{$kala->id}}/destroy" class="btn btn-danger ml-3">ایا از حذف مطمئن هستید؟</a>
<a href="/products/buy" class="btn btn-info ml-5">کنسل</a>
        </section>
    </section>
</section>
</div>
<br/>
@endsection
