@extends('layouts.master')
@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
    <nav aria-label="breadcrumb" >
        <ol class="breadcrumb ">
          <li class="breadcrumb-item "> <a href="#">خانه</a></li>
          <li class="breadcrumb-item "> <a href="#">مدیریت پرسنل</a></li>
          <li class="breadcrumb-item  active" aria-current="page">  ویرایش پرسنل </li>
        </ol>
      </nav>
    </div>
    <br>
      <section class="d-flex justify-content-start  " style="margin-right: 150px"">
        <h5>
     ویرایش پرسنل     </h5>
        <br><br>
    </section>

    <div class="container">
        <section class="row">
          <section class="col-12  col-md-6  m-auto ">

            @if($errors->any())
<div class="alert alert-danger text-right">{{$errors->first()}}</div>
@endif

<form method="post" action="/users/{{$user->username}}"  >
    @method('PUT')
    @csrf

    <div class="form-group row">
        <label for="fname" class="col-sm-2 col-form-label">نام</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="fname" name="fname" value="{{$user->fname}}" autocomplete="off" autofocus>
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-2 col-form-label">فامیلی </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="lname" name="lname" value="{{$user->lname}}" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label for="mobile" class="col-sm-2 col-form-label">شماره همراه</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="mobile" name="mobile" value="{{$user->mobile}}" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label">ایمیل</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" autocomplete="off">
        </div>
    </div>

    <div class="form-group row">
        <label for="mobile" class="col-sm-2 col-form-label">سمت </label>
        <section class="col-3">

            @foreach ($roles as $k=>$role)

               <div class="form-check ">
                   <input class="form-check-input" type="checkbox" name="role[]" id="role{{$k}}"  value="{{$role->id}}"   @foreach ($checked as $check) @if($check->id == $role->id) checked @endif @endforeach>

                   <label for="role{{$k}}" class="form-check-label mr-3 mt-1">{{$role->title}}</label>
                   <label>
               </div>

           </section>

           @endforeach

    </div>
    <div class="d-flex justify-content-center">
    <button type="submit" class="btn btn-primary">ویرایش</button>
    <a href="/users" class="btn btn-primary mr-5">کنسل</a>
</div>
</form>
@endsection
</section>
</section>
</div>
