@extends('layouts.master')
@section('pagetitle')
<h6>
<div class="d-flex justify-content-start  " style="margin-right: 150px">
    <nav aria-label="breadcrumb" >
        <ol class="breadcrumb ">
          <li class="breadcrumb-item "> <a href="#">خانه</a></li>
          <li class="breadcrumb-item "> <a href="#">مدیریت پرسنل</a></li>
          <li class="breadcrumb-item  active" aria-current="page">    لیست پرسنل

        </li>
        </ol>
      </nav>
    </div>
    <br>
      <section class="d-flex justify-content-start  " style="margin-right: 150px"">
        <h5>
            لیست پرسنل
          </h5>
        <br><br>
    </section>
</h6>

@endsection
@section('content')
<div class="container ">
    <div class="row">
        <div class="col-12  col-md-8  m-auto">

          <div class="d-flex justify-content-start col-5 ">

    <form method="post" action="/search" >@csrf
      <label for="fname"  >نام </label>
      <input type="text" name="fname" autocomplete="off" id="fname" autofocus>

          <label for="lname"  >فامیلی </label>
       <input type="text" name="lname" autocomplete="off" id="lname">
       <input type="submit" value="جستجو" >
   </form>
          </div>
    <br>
<table class="table table-striped  table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">نام</th>
      <th scope="col">نام خانوادگی</th>
      <th scope="col"><i class="fa fa-cogs"></i> تنظیمات</th>
    </tr>
  </thead>
  <tbody>
    @php($key=0)
    @foreach($users as $user)
    <tr>
      <td>{{$users->firstItem() + $key}}</td>
      <td>{{$user->fname}}</td>
      <td>{{$user->lname}}</td>
      <td>
        <a href="/users/{{$user->username}}/edit"><i class="fa fa-edit text-info" title="ویرایش"></i> </a>|
        <a href="/users/{{$user->username}}" onclick="event.preventDefault(); document.getElementById('form-delete-{{$user->username}}').submit();"><i class="fa fa-trash-alt text-danger" title="حذف"></i> </a>
        <form action="/users/{{$user->username}}" id="form-delete-{{$user->username}}" method="post">
          @method('DELETE')
          @csrf</form>
      </td>
    </tr>
    @php($key++)
    @endforeach
  </tbody>
</table>
<div class="d-flex justify-content-center">{!! $users->links( "pagination::bootstrap-4") !!}</div>
</div>

</div>
</div>
@endsection
