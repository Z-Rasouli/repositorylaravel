
@extends('layouts.master')
@section('pagetitle')
  {{--  پرسنل جدید  --}}
@endsection

@section('content')
<div class="d-flex justify-content-start  " style="margin-right: 150px">
<nav aria-label="breadcrumb" >
    <ol class="breadcrumb ">
      <li class="breadcrumb-item "> <a href="#">خانه</a></li>
      <li class="breadcrumb-item "> <a href="#">مدیریت پرسنل</a></li>
      <li class="breadcrumb-item  active" aria-current="page">  پرسنل جدید</li>
    </ol>
  </nav>
</div>
<br>
  <section class="d-flex justify-content-start  " style="margin-right: 150px"">
    <h5>
 پرسنل جدید    </h5>
    <br><br>
</section>
<div class="container">
  <section class="row">
    <section class="col-12  col-md-6  m-auto ">

<section>
@if($errors->any())
    <div class="alert alert-danger d-flex justify-content-start ">{{$errors->first()}}</div>
    @endif
</section>

<section class="container-fluid">

<form method="post" action="/users" enctype="multipart/form-data">
    @csrf
                    <section class="row">

                        <section class="col-12 col-md-6">
                            <div class="form-group text-right">
                                <label for="fname">نام<span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" id='fname' name="fname" value="{{old('fname')}}" autocomplete="off" placeholder="نام" autofocus>
                            </div>
                        </section>
                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="lname">نام خانوادگی<span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" id="lname" name="lname" value="{{old('lname')}}" autocomplete="off" placeholder="فامیلی">
                            </div>
                        </section>
                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="email">ایمیل<span class="text-danger">*</span></label>
                                <input type="email" class="form-control form-control-sm" id='email' name="email" value="{{old('email')}}" autocomplete="off" placeholder="email@example.com">
                            </div>
                        </section>
                     <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="username"> نام کاربری<span class="text-danger">*</span> </label>
                                <input type="text" class="form-control form-control-sm"  id="username" name="username" value="{{old('username')}}" autocomplete="off" placeholder="کدملی">
                            </div>
                        </section>
                     <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="password">کلمه عبور<span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-sm" name='password' id='password' placeholder="گذره واژه">
                            </div>
                        </section>
                    <section class="col-12 col-md-6">
                            <div class="form-  text-right">
                                <label for="password_confirmation">تکرار کلمه عبور<span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-sm" name='password_confirmation' id='password_confirmation' placeholder="تکرار گذره واژه ">
                            </div>
                        </section>

                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="mobile"> مبایل <span class="text-danger">*</span> </label>
                                <input type="text" class="form-control form-control-sm"  id="mobile" name="mobile" value="{{old('mobile')}}" autocomplete="off" placeholder="09380874586">
                            </div>
                        </section>
                        <section class="col-12 col-md-6">
                            <div class="form-group  text-right">
                                <label for="">نام انبار</label>
                                <select name="anbar" id="anbar" class="form-control form-control-sm">
                                    <option value="{{$repositories->title}}"  @if (old('anbar') == $repositories->title) selected="selected" @endif>{{$repositories->title}}</option>
                             </select>
                            </div>
                        </section>

                        <div class="col-12 text-right">
                            <label for="" class="">سمت</label>
                        </div>
                        <section class="col-3">
                         @foreach ($roles as $k=>$role)
                            <div class="form-check ">
                                <input class="form-check-input" type="checkbox" name="role[]" id="role{{$k}}"  value="{{$role->id}}" @if(is_array(old('role')) && in_array($role->id, old('role'))) checked @endif>
                                <label for="role{{$k}}" class="form-check-label mr-3 mt-1">{{$role->title}}</label>
                                <label>
                            </div>
                        </section>
                        @endforeach

                        <section class="col-12">
                            <button class="btn btn-primary btn-sm">ثبت</button>
                        </section>

                    </section>
                </form>



        </section>
    </section>
</section>
</div>



@endsection
