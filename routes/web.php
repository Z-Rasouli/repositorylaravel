<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home','dashboardcontroller@index')->middleware('auth');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'dashboardcontroller@index');
    Route::get('/home', 'dashboardcontroller@index');
    Route::post('/search','dashboardcontroller@search');
    Route::resource('/users', 'userController');
    Route::prefix('/products')->group(function(){
    Route::get('/buy', 'productcontroller@create');
    Route::get('/newKala', 'productcontroller@SowNewKala');
    Route::any('/savenewKala', 'productcontroller@savenewKala');
    Route::any('/search', 'productcontroller@search')->name('search');
    Route::any('/kalaSearch', 'productcontroller@kalaSearch');
    Route::any('/save', 'productcontroller@store');
    Route::get('buy/{id}/edit', 'productcontroller@edit');
    Route::post('buy/update', 'productcontroller@update')->name('update');
    Route::any('buy/{id}/info_del', 'productcontroller@show');
    Route::any('buy/{id}/destroy', 'productcontroller@destroy');
    Route::any('/exit', 'ExitkalaController@create');
    Route::any('/save_ExitKala', 'ExitkalaController@store');
    Route::any('/search_ExitKala', 'ExitkalaController@search_ExitKala')->name('search');
    Route::any('/search_kalaExit', 'ExitkalaController@kalaExit');
    Route::any('/cash', 'cashcontroller@cash');
    Route::any('/', 'cashcontroller@code_searchCash');
    Route::any('/cash_search', 'cashcontroller@cash_search')->name('cash_search');
});
Route::prefix('/Estekhdami')->group(function(){
    Route::get('/request', 'Estekhdamicontroller@request');
    Route::post('/saveRequest', 'Estekhdamicontroller@store');
    Route::get('/vizit', 'UserEstekhdamcontroller@vizit');
    Route::any('/saveVizit', 'UserEstekhdamcontroller@saveVizit');
});
});
Route::prefix('/Estekhdami')->group(function(){
Route::get('/requisition_person', 'UserEstekhdamcontroller@requisition_person');
Route::get('/{id}/requisition_save', 'UserEstekhdamcontroller@requisition_save');
Route::get('/check', 'UserEstekhdamcontroller@check');
Route::post('/save_user', 'UserEstekhdamcontroller@save_user');
Route::post('/code_meli_befor', 'UserEstekhdamcontroller@code_meli_befor'); //check code_meli befor rigister
Route::post('/save_formEstakhdami', 'UserEstekhdamcontroller@save_formEstakhdami');
Route::post('/vizit/searchState', 'UserEstekhdamcontroller@searchState');

});

Route::get('/t','dashboardcontroller@test');



// Route::get('/',function () {

//     return view('si');

// });


// Route::prefix('ss/a/s')->group(function(){
//     Route::get('art','article@index');
// Route::get ('/a/{id?}','article@edit');
// Route::get('/{a}/{b}', function ($a,$b) {
//     return $a.'->'.$b;
// });
// });


