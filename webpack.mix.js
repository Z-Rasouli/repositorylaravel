const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);





mix.styles(
    [
        "resources/css/app.css",
        "resources/css/bootstrap.css",
        "resources/css/grid.css",
    "resources/css/fontiran.css",
    "resources/css/fontstyle.css"
],
    "public/css/admin.css"
).version();


mix.scripts(
    [
        "resources/js/jquery-3.5.1.min.js",
        "resources/js/bootstrap.js",
        // "resources/js/jquery-3.3.1.min.js",
        "resources/js/popper.js",
        "resources/js/popper-3.5.1.js",
        "resources/js/grid.js",
        "resources/js/script.js",
],
    "public/js/admin.js"
).version();

